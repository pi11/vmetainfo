from setuptools import setup

setup(name='vmetainfo',
      version='0.0.1',
      description='Get video meta information from video/audio file using ffprobe/avprobe',
      url='https://bitbucket.org/pi11/vmetainfo',
      author="pi11",
      author_email="webii@pm.me"
      )
